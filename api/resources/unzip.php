<?php
$get = function ()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->body->zip = realpath('/srv/' . $input->body->zip);

	if (!is_file($input->body->zip))
		return array("code" => 404, "message" => "Le fichier zip spécifié n'existe pas : " . $input->body->zip);

	if (substr($input->body->zip,0,11) != '/srv/files/')
		return array("code" => 404, "message" => "Le chemin spécifié conduit à un dossier dont l'accès est interdit");

	$path = explode('/', $input->body->zip);
	$input->owner = get_user_id($path[3]);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'files/' . substr($input->body->zip,11));
		if (in_array('read', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire ce fichier");
	}


	$input->body->path = realpath(substr($input->body->zip, 0, strrpos($input->body->zip,'/')));

	$path = explode('/', $input->body->path);
	$input->owner = get_user_id($path[3]);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'files/' . substr($input->body->path,11));
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour écrire dans ce dossier");
	}

	$output_path = substr($input->body->zip, 0 ,-4);
	$output_parent_folder = explode('/', $output_path);
	array_pop($output_parent_folder);
	$output_parent_folder = implode('/', $output_parent_folder);
	$output_folder = end(explode('/', $output_path));

	if (file_exists($output_path))
		return array("code" => 409, "message" => "Opération impossible : un dossier nommé " . $output_folder . " existe déjà");
	else
		mkdir($output_path);

	exec("unzip -n '" . $input->body->zip . "' -d '" . $output_path . "' 2>&1", $output);
	
	return array("code" => 200);
}
?>