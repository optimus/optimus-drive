<?php
$get = function ()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->body->file = realpath('/srv/' . $_GET['file']);

	if (!is_file($input->body->file))
		return array("code" => 404, "message" => "Le fichier spécifié n'existe pas : " . $_GET['file']);

	if (substr($input->body->file,0,11) != '/srv/files/')
		return array("code" => 404, "message" => "Le chemin spécifié conduit à un dossier dont l'accès est interdit");

	$path = explode('/', $input->body->file);
	$input->owner = get_user_id($path[3]);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'files/' . substr($input->body->file,11));
		if (in_array('read', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à ce fichier");
	}

	header('Content-type: ' . mime_content_type($input->body->file)); 
	header('Content-Disposition: attachment; filename="' . end(explode('/', $input->body->file)) . '"'); 
	header('Content-Transfer-Encoding: binary'); 
	header("Content-Length: " . filesize($input->body->file));
	
	@readfile($input->body->file); 
};
?>
