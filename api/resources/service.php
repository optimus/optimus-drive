<?php
$get = function ()
{
	global $connection;
	auth();
	allowed_origins_only();
	$service_name = 'optimus-drive';

	$query = $connection->prepare("SELECT status, manifest FROM `server`.`services` WHERE name = :name");
	$query->bindParam(":name", $service_name);
	$query->execute();
	$service = $query->fetch(PDO::FETCH_OBJ);
	$output = json_decode($service->manifest, false);
	$output->status = $service->status;

	return array("code" => 200, "data" => $output);
};


$post = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$service_name = 'optimus-drive';

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut activer ce service");
	
	$is_installed = $connection->prepare("SELECT * FROM server.users_services WHERE user = '" . $input->owner . "' AND service = '" . $service_name . "'");
	$is_installed->execute();
	if ($is_installed->rowCount() != 0)
		return array("code" => 400, "message" => "Le service " . $service_name . " est déjà activé pour cet utilisateur");
		
	if(!$connection->query("USE `user_" . $input->owner . "`"))
		$errors[] = $connection->errorInfo()[2];
	if (!$connection->query("REPLACE INTO `server`.`users_services` SET user = '" . $input->owner . "', service = '" . $service_name . "'"))
		$errors[] = $connection->errorInfo()[2];
	$sql_files = array_diff(scandir('/srv/sql/user'), array('..', '.'));
	
	foreach($sql_files as $sql_file)
	{
		$sql = file_get_contents("/srv/sql/user/" . $sql_file);
		$sql = explode(';',$sql);
		foreach ($sql as $instruction)
			if($instruction!='')
				if (!$connection->query($instruction))
					$errors[] = $connection->errorInfo()[2];
	}

	umask(0);
	if (!file_exists('/srv/files/') . get_user_email($input->owner))
		@mkdir('/srv/files/' . get_user_email($input->owner));
	@chmod('/srv/files/' . get_user_email($input->owner), 0770);

	if ($errors)
		return array("code" => 400, "message" => $errors);
	else
		return array("code" => 201);
};

$patch = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$service_name = 'optimus-drive';

	if (isset($input->body->email))
		$input->body->email = strtolower($input->body->email);

	$input->body->email = check('email', $input->body->email, 'email', true);
	
	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul un administrateur ou l'utilisateur lui même peuvent modifier un utilisateur");

	if (!exists($connection, 'server','users', 'id', $input->owner))
		return array("code" => 404, "message" => "Erreur - cet utilisateur n'existe pas");
	
	if (exists($connection, 'server','users', 'email', $input->body->email))
		return array("code" => 409, "message" => "Erreur - un utilisateur avec cette adresse email existe déjà");
	
	if (isset($input->body->email) && $input->body->email != get_user_email($input->owner))
		if (is_dir('/srv/files/' . get_user_email($input->owner)))
			rename('/srv/files/' . get_user_email($input->owner), '/srv/files/' . $input->body->email);
	
	return array("code" => 200);
};

$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$service_name = 'optimus-drive';

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut désactiver un service");

	if ($_GET['remove_data'] == true)
	{
		if(!$connection->query("USE `user_" . $input->owner . "`"))
			$errors[] = $connection->errorInfo()[2];
		
		//SUPPRESSION DES BASES UTILISATEURS
		$sql = file_get_contents("/srv/sql/user_uninstall.sql");
		$sql = explode(';',$sql);
		foreach ($sql as $instruction)
			if($instruction!='')
				if (!$connection->query($instruction))
					$errors[] = $connection->errorInfo()[2];
		
		//SUPPRESSION DES PARTAGES
		if (!$connection->query("DELETE FROM server.authorizations WHERE owner = '" . $input->owner . "' AND (resource = 'files' OR resource LIKE 'files/%')"))
			$errors[] = $connection->errorInfo()[2];
	}
		
	if (!$connection->query("DELETE FROM `server`.`users_services` WHERE user = '" . $input->owner . "' AND service = '" . $service_name . "'"))
		$errors[] = $connection->errorInfo()[2];
	
	if ($errors)
		return array("code" => 400, "message" => $errors);
	else
		return array("code" => 200);
};
?>