<?php
$get = function ()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$pathinfo = pathinfo('/srv/' . $input->body->file);

	if (!is_dir($pathinfo['dirname']))
		return array("code" => 404, "message" => "Le dossier spécifié n'existe pas : " . $pathinfo['dirname']);

	if (substr($pathinfo['dirname'],0,11) != '/srv/files/')
		return array("code" => 404, "message" => "Le chemin spécifié conduit à un dossier dont l'accès est interdit");

	$path = explode('/', $pathinfo['dirname']);
	$input->owner = get_user_id($path[3]);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'files/' . substr($pathinfo['dirname'],11));
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à ce dossier");
	}


	if (strtolower($pathinfo['extension']) != 'zip')
	{
		$pathinfo['filename'] = $pathinfo['filename'] . '.' . $pathinfo['extension'];
		$pathinfo['extension'] = 'zip';
		$pathinfo['basename'] = $pathinfo['basename'] . '.' . $pathinfo['extension'];
	}

	if (is_file($pathinfo['dirname'] . '/' . $pathinfo['basename']))
		return array("code" => 409, "message" => "Opération impossible : un fichier ou dossier nommé " . $pathinfo['basename'] . " existe déjà");

	if (!isset($input->body->content)) 
		return array("code" => 400, "message" => "Aucun fichier ou dossier à compresser n'a été spécifié");

	$nodes = [];
	foreach ($input->body->content as $node)
	{
		$node = realpath('/srv/' . $node);

		if (!file_exists($node))
			return array("code" => 404, "message" => "Un des fichiers ou dossiers spécifiés n'existe pas : " . $node);

		if (substr($node,0,11) != '/srv/files/')
			return array("code" => 404, "message" => "Le chemin spécifié conduit à un dossier dont l'accès est interdit");

		$path = explode('/', $node);
		$node_owner = get_user_id($path[3]);

		if ($input->user->id != $input->owner AND !is_admin($input->user->id))
		{
			$restrictions = get_restrictions($input->user->id, $node_owner, 'files/' . substr($node,11));
			if (in_array('read', $restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire ce dossier ou fichier");
		}

		$nodes[] = end(explode('/', $node));
	}

	//print_r($nodes);exit;
	
	exec("LC_ALL=fr_FR.UTF-8 export HOME=/srv/tmp && cd '" . $pathinfo['dirname'] . "'; zip -r '" . $pathinfo['basename'] . "' '" . implode("' '", $nodes) . "' 2>&1", $output);
	
	if (is_file($pathinfo['dirname'] .  '/' . $pathinfo['basename']))
		return array("code" => 200, "data" => array("filename" => $pathinfo['basename'], "size" => filesize($pathinfo['dirname'] . '/' . $pathinfo['basename']), "lastmodified" => date('Y-m-d\TH:i:s', filemtime($pathinfo['dirname'] . '/' . $pathinfo['basename']))), "message" => "Conversion effectuée avec succès");
	else
		return array("code" => 400, "message" => "La conversion a échoué");
};
?>
