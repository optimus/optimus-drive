<?php
$get = function ()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->body->path = realpath('/srv/' . $input->body->path);

	if (!is_dir($input->body->path))
		return array("code" => 404, "message" => "Le dossier spécifié n'existe pas : " . $input->body->path);

	if (substr($input->body->path,0,10) != '/srv/files')
		return array("code" => 403, "message" => "Le chemin spécifié conduit à un dossier dont l'accès est interdit");

	$results = [];
	$input->body->path = $input->body->path;
	$directoryIterator = new RecursiveDirectoryIterator($input->body->path);
	$iteratorIterator = new RecursiveIteratorIterator($directoryIterator);
	$x = 0;
	foreach ($iteratorIterator as $file) 
	{
		$split_path = explode('/', $file);
		if (!$split_path[3] OR $split_path[3] == 'tmp' OR end($split_path) == '..')
			continue;
		else if(end($split_path) == '.')
		{
			$type = 'folder';
			//array_shift($split_path);
			//array_shift($split_path);
			//array_pop($split_path);
			$nodename = array_pop($split_path);
		}
		else
		{
			$type = 'file';
			$nodename = end($split_path);
		}

		if (preg_match('/' . $input->body->search . '/i', $nodename))
		{
			$path = implode('/', $split_path);
			$input->owner = get_user_id($split_path[3]);

			if ($input->user->id != $input->owner AND !is_admin($input->user->id))
			{
				$restrictions = get_restrictions($input->user->id, $input->owner, 'files/' . substr($path,11));
				if (in_array('read', $restrictions))
					continue;
			}

			if($type == 'folder')
				$results[] = array(
						'id' => $x++,
						'path' => implode('/', $split_path),
						'type' => 'folder',
						'displayname' => $nodename,
						'size' => null,
						'lastmodified' => date('Y-m-d H:i:s', filemtime($file)),
					);
			else
				$results[] = array(
					'id' => $x++,
					'path' => substr($file, 5, strrpos($file, '/')-5),
					'type' => 'file',
					'displayname' => $nodename,
					'size' => filesize($file),
					'lastmodified' => date('Y-m-d H:i:s', filemtime($file)),
				);
		}

		if (isset($input->body->size) AND sizeof($results) == $input->body->size)
			break;
	}

	return array("code" => 200, "data" => $results);
};
?>
