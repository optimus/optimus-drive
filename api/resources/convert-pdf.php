<?php
$get = function ()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->body->file = realpath('/srv/' . $input->body->file);

	if (!is_file($input->body->file))
		return array("code" => 404, "message" => "Le fichier spécifié n'existe pas : " . $input->body->file);

	if (substr($input->body->file,0,11) != '/srv/files/')
		return array("code" => 404, "message" => "Le chemin spécifié conduit à un dossier dont l'accès est interdit");

	$path = explode('/', $input->body->file);
	if ($path[3] != 'tmp')
		$input->owner = get_user_id($path[3]);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'files/' . substr($input->body->file,11));
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à ce fichier");
	}

	$path = substr($input->body->file,0,strrpos($input->body->file,'/'));
	$filename = end(explode('/', $input->body->file));
	$extension = end(explode('.', $filename));
	$radical = substr($filename, 0, -strlen($extension) - 1);

	if (is_file(substr($input->body->file,0,strrpos($input->body->file,'.')) . '.pdf'))
		return array("code" => 409, "message" => "Opération impossible : un fichier nommé " . $radical . ".pdf existe déjà");

	exec("LC_ALL=fr_FR.UTF-8 export HOME=/srv/tmp && libreoffice --headless --convert-to pdf:writer_pdf_Export '" . $input->body->file . "' --outdir '" . $path . "' 2>&1", $output);

	if (is_file(substr($input->body->file,0,strrpos($input->body->file,'.')) . '.pdf'))
		return array("code" => 200, "data" => array("filename" => $radical . ".pdf", "size" => filesize($path . '/' . $radical . '.pdf'), "lastmodified" => date('Y-m-d\TH:i:s', filemtime($path . '/' . $radical . '.pdf'))) ,"message" => "Conversion en PDF effectuée avec succès");
	else
		return array("code" => 400, "message" => "La conversion en PDF a échoué" . implode("\n", $output));
};
?>
