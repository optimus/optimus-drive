export default class OptimusDriveExplorerIndex
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/components/optimus_webdav_explorer/index.js', main,
			{
				id: 'files',
				title: 'Fichiers',
				resource: 'files',
				server: 'https://' + store.user.server.replace('api.', 'cloud.'),
				root: 'files',
				path: '',
				cannotGetHigherThan: null,
				showBreadcrumb: true,
				showSearchBox: true,
			}
		)
	}
}
