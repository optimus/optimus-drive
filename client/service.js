export default class OptimusDriveService
{
	constructor()
	{
		this.name = 'optimus-drive'
		this.resources = [
			{
				id: 'files',
				name: 'fichiers',
				description: 'Tous les fichiers',
				path: 'files'
			},
			// {
			// 	id: 'file',
			// 	name: 'fichier',
			// 	description: 'Un fichier déterminé',
			// 	path: 'files/id',
			// 	endpoint: '/optimus-drive/{owner}/files'
			// }
		]
		this.swagger_modules =
			[
				{
					id: "optimus-drive",
					title: "OPTIMUS DRIVE",
					path: "/services/optimus-drive/optimus-drive.yaml",
					filters: ["file-operations", "service"]
				}
			]
		store.services.push(this)
	}

	login()
	{
		leftmenu.create('optimus-cloud', 'CLOUD')
		leftmenu['optimus-cloud'].add('files', 'Fichiers', 'fas fa-hard-drive', () => router('optimus-drive/explorer?path=' + encodeURIComponent(store.user.email)))

		store.resources = store.resources.concat(this.resources)
	}

	logout()
	{
		leftmenu['optimus-cloud'].querySelector('#files').remove()
		for (let resource of this.resources)
			store.resources.splice(store.resources.findIndex(item => item.id == resource.id), 1)
		store.services = store.services.filter(service => service.name != this.id)
	}

	async global_search(search_query)
	{
		return [
			{
				icon: 'fas fa-file',
				title: 'Fichiers',
				data: rest(store.user.server + '/optimus-drive/search', 'GET', { search: search_query, path: 'files', size: 20 })
					.then(nodes => nodes?.data?.map(node => 
					{
						return {
							colorclass: node.type == 'folder' ? 'is-warning' : 'is-info',
							title: node.path + '/' + node.displayname,
							displayname: node.displayname,
							route: 'optimus-drive/explorer?path=' + encodeURIComponent(node.path.substring(5)) + (node.type == 'folder' ? '/' + encodeURIComponent(node.displayname) : '')
						}
					}))
			}
		]
	}

	dashboard()
	{
		//if (!document.getElementById('quickaction-cloud-header'))
		//quickactions_add('quickaction-cloud-header', 'header', 'CLOUD')
		//quickactions_add('quickaction-cloud-event-create', 'item', 'Créer un nouvel évènement', 'fas fa-calendar-plus', () => quickactionEventCreate())
		//fetch('/services/optimus-calendar/dashboard.html').then(response => response.text()).then(response => document.getElementById('dashboard').insertAdjacentHTML('beforeend', response))
	}
}