<?php
header("Access-Control-Allow-Origin: " . (isset($_SERVER['HTTP_ORIGIN'])?$_SERVER['HTTP_ORIGIN']:$_SERVER['SERVER_NAME']));
header("Access-Control-Allow-Methods: OPTIONS,GET,HEAD,DELETE,PROPFIND,PUT,PROPPATCH,COPY,MOVE,REPORT,MKCOL,POST,LOCK,UNLOCK");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Authorization, Digest, Content-Type, Credentials, Depth, Destination, Overwrite, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header("Access-Control-Max-Age: 5");

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS" AND isset($_SERVER['HTTP_ORIGIN']) AND isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
	die(http_response_code(200));

include('/srv/api/libs/JWT.php');
require('/srv/sabredav/vendor/autoload.php');
include('/srv/sabredav/optimus/DAV/FSExt/Directory.php');

$pdo = new PDO("mysql:host=optimus-databases;dbname=cloud", getenv('MARIADB_API_USER'), getenv('MARIADB_API_PASSWORD'));
$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

$connection = $pdo;

function exception_error_handler($errno, $errstr, $errfile, $errline) 
{
	error_log($errno . ' / ' . $errstr. ' / ' . $errfile. ' / ' . $errline);
	throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler("exception_error_handler");

if (isset($_COOKIE['token']))
	$authBackend = new Optimus\DAV\Auth\Backend\JWTCookie;
else
	$authBackend = new Optimus\DAV\Auth\Backend\PDO($pdo);

$principalBackend = new Optimus\DAVACL\PrincipalBackend\PDO($pdo);
//$caldavBackend = new Sabre\CalDAV\Backend\PDO($pdo);
//$carddavBackend   = new Optimus\CardDAV\Backend\PDO($pdo);
$lockBackend = new Sabre\DAV\Locks\Backend\PDO($pdo);
$storageBackend = new Sabre\DAV\PropertyStorage\Backend\PDO($pdo);


$nodes = 
[
	new Sabre\DAVACL\PrincipalCollection($principalBackend),
	new Optimus\DAVACL\FS\HomeCollection($principalBackend, '/srv/files'),
	//new Sabre\CalDAV\CalendarRoot($principalBackend, $caldavBackend),
	//new Sabre\CardDAV\AddressBookRoot($principalBackend, $carddavBackend),
];

$server = new Sabre\DAV\Server($nodes);

$server->addPlugin(new Sabre\DAV\Auth\Plugin($authBackend));
$server->addPlugin(new Sabre\DAV\Locks\Plugin($lockBackend));
$server->addPlugin(new Sabre\DAV\PropertyStorage\Plugin($storageBackend));
$server->addPlugin(new Sabre\DAV\Browser\Plugin());
$server->addPlugin(new Sabre\DAV\Sharing\Plugin());
$server->addPlugin(new Sabre\DAV\Sync\Plugin());
//$server->addPlugin(new Sabre\CalDAV\Plugin());
//$server->addPlugin(new Sabre\CalDAV\Schedule\Plugin());
//$server->addPlugin(new Sabre\CalDAV\SharingPlugin());
//$server->addPlugin(new Sabre\CalDAV\ICSExportPlugin());
//$server->addPlugin(new Sabre\CardDAV\Plugin());
//$server->addPlugin(new Sabre\CardDAV\VCFExportPlugin());

$aclPlugin = new Sabre\DAVACL\Plugin();
$aclPlugin->allowAccessToNodesWithoutACL = false;
$aclPlugin->hideNodesFromListings = true;
$server->addPlugin($aclPlugin);

// $server->on('exception', function(Exception $e) 
// {
// 	if ($e instanceof \Sabre\DAV\Exception\NotAuthenticated)
// 		error_log($_SERVER['REMOTE_ADDR'] . ' - ' . $e->getMessage());
// });

$server->exec();
?>