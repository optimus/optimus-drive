<?php
namespace Optimus\DAV\Auth\Backend;

use Sabre\HTTP;
use Sabre\HTTP\RequestInterface;
use Sabre\HTTP\ResponseInterface;

include_once '/srv/api/libs/JWT.php';
use optimus\JWT;

class JWTCookie extends \Sabre\DAV\Auth\Backend\AbstractBearer
{
	public function validateBearerToken($bearerToken)
	{
		return [false, "Invalid Token"];
	}

	public function check(RequestInterface $request, ResponseInterface $response)
	{
		try
		{
			$payload = (new JWT(getenv('API_SHA_KEY'), 'HS512', 3600, 10))->decode($_COOKIE['token']);
			$this->token = $_COOKIE['token'];
			return [true, "principals/" . $payload['user']->email];
		}
		catch (Throwable $e)
		{
			return [false, "Invalid Token"];
		}
	}

	public function challenge(RequestInterface $request, ResponseInterface $response)
	{

	}

}
