<?php

declare(strict_types=1);

namespace Optimus\DAV\Auth\Backend;

use optimus\JWT;

class PDO extends \Sabre\DAV\Auth\Backend\AbstractBasic
{
	public function __construct()
	{
	}

	public function validateUserPass($username, $password)
	{
		include_once('/srv/api/libs/functions.php');
		$input = '{"email":"' . $username . '", "password":"' . $password . '"}';
		$output = rest('https://api.' . getenv('DOMAIN') . '/optimus-base/login', 'POST', $input, null);
		if ($output->code == 200)
			$this->token = $output->cookies->token;
		else
			return false;
		
		try
		{
			$payload = (new JWT(getenv('API_SHA_KEY'), 'HS512', 3600, 10))->decode($this->token);
			return true;
		}
		catch (Throwable $e)
		{
			return false;
		}
	}
}
