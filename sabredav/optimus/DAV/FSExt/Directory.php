<?php
declare(strict_types=1);

namespace Sabre\DAV\FSExt;

use Sabre\DAV;
use Sabre\DAV\FS\Node;

class Directory extends Node implements DAV\ICollection, DAV\IQuota, DAV\IMoveTarget
{
	public function createFile($name, $data = null)
	{
		if ('.' == $name || '..' == $name)
			throw new DAV\Exception\Forbidden('Permission denied to . and ..');

		$newPath = $this->path.'/'.$name;
		$path = explode('/', $newPath);

		global $server, $pdo, $authBackend;
		include_once('/srv/api/libs/functions.php');
		$owner_email = check('owner_email', $path[3], 'email', true);
		$user_email = check('owner_email', str_replace('principals/', '', $server->getPlugin('auth')->getCurrentPrincipal()), 'email', true);
		$owner_id = get_user_id($owner_email);
		$user_id = get_user_id($user_email);
		$admin = is_admin($user_id);

		//SI L'UTILISATEUR EST ADMIN, OU SI L'UTILISATEUR NAVIGUE DANS SES PROPRES FICHIERS, AUCUNE VERIFICATION N'EST NECESSAIRE
		if (!is_admin($user_id) AND $owner_id != $user_id)
		{
			$users_services = rest('https://api.' . getenv('DOMAIN') . '/optimus-base/users/' . $user_id . '/services', 'GET', null, $authBackend->token)->data;
			foreach($users_services as $service)
				if (is_array($service->managed_resources) AND in_array($path[4], $service->managed_resources))
					$current_service = $service->name;
			
			//HYPOTHESE 1 : ON EST DANS UN DOSSIER DE TYPE 'resource'
			if (isset($current_service))
			{
				if (sizeof($path) == 6)
					throw new DAV\Exception\Forbidden('Permission denied to create in a resource folder. Please use API');
				else if (sizeof($path) > 6)
				{
					$input = '{"filter": [{"field":"displayname", "type":"=", "value":"' . $path[5] . '"}]}';
					$resource = rest('https://api.' . getenv('DOMAIN') . '/' . $current_service . '/' . $owner_id . '/' . $path[4], 'GET', $input, $authBackend->token);
					if ($resource->code != 200 OR sizeof($resource->data) == 0)
						throw new DAV\Exception\Forbidden('Permission denied. Unknown resource');
					else if (in_array('write', get_restrictions($user_id, $owner_id, $path[4] . '/' . $resource->data[0]->id)))
						throw new DAV\Exception\Forbidden('Permission denied to write this resource');
				}
			}
			//HYPOTHESE 2 : ON EST DANS UN DOSSIER DE TYPE 'files'
			else
			{
				//ON VERIFIE S'IL EXISTE UNE AUTHORIZATION DE TYPE 'files'
				$resource_path = explode('/', $this->path);
				unset($resource_path[0]); unset($resource_path[1]); unset($resource_path[3]);
				$resource_path = implode('/', $resource_path);
				if (in_array('write', get_restrictions($user_id, $owner_id, $resource_path)))
					throw new DAV\Exception\Forbidden('Permission denied');
			}
		}
		
		file_put_contents($newPath, $data);
		clearstatcache(true, $newPath);

		return '"'.sha1(fileinode($newPath) . filesize($newPath) . filemtime($newPath)) .'"';
	}

	public function createDirectory($name)
	{
		if ('.' == $name || '..' == $name)
			throw new DAV\Exception\Forbidden('Permission denied to . and ..');
		
		$newPath = $this->path . '/' . $name;
		$path = explode('/', $newPath);

		global $server, $pdo, $authBackend;
		include_once('/srv/api/libs/functions.php');
		$owner_email = check('owner_email', $path[3], 'email', true);
		$user_email = check('owner_email', str_replace('principals/', '', $server->getPlugin('auth')->getCurrentPrincipal()), 'email', true);
		$owner_id = get_user_id($owner_email);
		$user_id = get_user_id($user_email);
		$admin = is_admin($user_id);

		//SI L'UTILISATEUR EST ADMIN, OU SI L'UTILISATEUR NAVIGUE DANS SES PROPRES FICHIERS, AUCUNE VERIFICATION N'EST NECESSAIRE
		if (!is_admin($user_id) AND $owner_id != $user_id)
		{
			$users_services = rest('https://api.' . getenv('DOMAIN') . '/optimus-base/users/' . $user_id . '/services', 'GET', null, $authBackend->token)->data;
			foreach($users_services as $service)
				if (is_array($service->managed_resources) AND in_array($path[4], $service->managed_resources))
					$current_service = $service->name;
			
			//HYPOTHESE 1 : ON EST DANS UN DOSSIER DE TYPE 'resource'
			if (isset($current_service))
			{
				if (sizeof($path) == 6)
					throw new DAV\Exception\Forbidden('Permission denied to create in a resource folder. Please use API');
				else if (sizeof($path) > 6)
				{
					$input = '{"filter": [{"field":"displayname", "type":"=", "value":"' . $path[5] . '"}]}';
					$resource = rest('https://api.' . getenv('DOMAIN') . '/' . $current_service . '/' . $owner_id . '/' . $path[4], 'GET', $input, $authBackend->token);
					if ($resource->code != 200 OR sizeof($resource->data) == 0)
						throw new DAV\Exception\Forbidden('Permission denied. Unknown resource');
					else if (in_array('write', get_restrictions($user_id, $owner_id, $path[4] . '/' . $resource->data[0]->id)))
						throw new DAV\Exception\Forbidden('Permission denied to write this resource');
				}
			}
			//HYPOTHESE 2 : ON EST DANS UN DOSSIER DE TYPE 'files'
			else
			{
				//ON VERIFIE S'IL EXISTE UNE AUTHORIZATION DE TYPE 'files'
				$resource_path = explode('/', $this->path);
				unset($resource_path[0]); unset($resource_path[1]); unset($resource_path[3]);
				$resource_path = implode('/', $resource_path);
				if (in_array('write', get_restrictions($user_id, $owner_id, $resource_path)))
					throw new DAV\Exception\Forbidden('Permission denied');
			}
		}
		
		mkdir($newPath);
		clearstatcache(true, $newPath);
	}

	public function getChild($name)
	{
		$path = $this->path.'/'.$name;

		if (!file_exists($path))
			throw new DAV\Exception\NotFound('File could not be located');
		if ('.' == $name || '..' == $name)
			throw new DAV\Exception\Forbidden('Permission denied to . and ..');

		if (is_dir($path))
			return new self($path);
		else
			return new File($path);
	}

	public function childExists($name)
	{
		if ('.' == $name || '..' == $name)
			throw new DAV\Exception\Forbidden('Permission denied to . and ..');
		$path = $this->path.'/'.$name;

		return file_exists($path);
	}

	public function getChildren()
	{
		$nodes = [];
		$path = explode('/', $this->path);

		global $server, $pdo, $authBackend;
		include_once('/srv/api/libs/functions.php');
		$owner_email = check('owner_email', $path[3], 'email', true);
		$user_email = check('owner_email', str_replace('principals/', '', $server->getPlugin('auth')->getCurrentPrincipal()), 'email', true);
		$owner_id = get_user_id($owner_email);
		$user_id = get_user_id($user_email);
		$admin = is_admin($user_id);

		//SI L'UTILISATEUR EST ADMIN, OU SI L'UTILISATEUR NAVIGUE DANS SES PROPRES FICHIERS, ON LISTE TOUT LE CONTENU SANS RESTRICTION
		if (is_admin($user_id) OR $owner_id == $user_id)
		{
			$iterator = new \FilesystemIterator($this->path, \FilesystemIterator::CURRENT_AS_SELF | \FilesystemIterator::SKIP_DOTS);
			foreach ($iterator as $entry)
				$nodes[] = $this->getChild($entry->getFilename());
			return $nodes;
		}
		
		//SI L'UTILISATEUR BENEFICIE D'UN PARTAGE 'files' SUR LE DOSSIER COURANT, ON LISTE TOUT LE CONTENU SANS RESTRICTION
		$resource_path = explode('/', $this->path);
		unset($resource_path[0]); unset($resource_path[1]); unset($resource_path[3]);
		$resource_path = implode('/', $resource_path);
		$restrictions = get_restrictions($user_id, $owner_id, $resource_path);
		if (!in_array('read', $restrictions))
		{
			$iterator = new \FilesystemIterator($this->path, \FilesystemIterator::CURRENT_AS_SELF | \FilesystemIterator::SKIP_DOTS);
			foreach ($iterator as $entry)
				$nodes[] = $this->getChild($entry->getFilename());
			return $nodes;
		}

		//DANS LES AUTRES CAS, ON AFFICHE UNIQUEMENT LES ELEMENTS AUXQUELS L'UTILISATEUR EST AUTORISE A ACCEDER EN LECTURE

		//POUR TOUS LES PARTAGES DE TYPE 'files/...' IL FAUT AFFICHER LES DOSSIERS PARENTS, SANS QUOI L'UTILISATEUR NE POURRA PAS NAVIGUER JUSQU'AU DOSSIER OU FICHIER PARTAGE
		$sub_authorizations = $pdo->prepare("SELECT resource FROM server.authorizations WHERE user = " . $user_id . " AND owner = " . $owner_id . " AND resource LIKE :resource_path AND `read` = 1");
		$param = $resource_path . '%';
		$sub_authorizations->bindParam(':resource_path', $param);
		$sub_authorizations->execute();
		while($resource = $sub_authorizations->fetchObject())
			if (strpos($resource->resource, $resource_path) > -1)
				$nodes[] = $this->getChild(explode('/', $resource->resource)[sizeof(explode('/', $resource_path))]);

		//AU NIVEAU 3, ON AFFICHE LES DOSSIERS QUI CONTIENNENT DES RESSOURCES AUXQUELLES L'UTILISATEUR PEUT ACCEDER EN LECTURE EN APPLICATION D'AU MOINS UN PARTAGE EXISTANT
		if (end($path) == $path[3])
		{
			$authorized_resources = $pdo->query("SELECT DISTINCT SUBSTRING_INDEX(resource, '/',1) as resource FROM server.authorizations WHERE user = " . $user_id . " AND owner = " . $owner_id . " AND `read` = 1");
			while($resource = $authorized_resources->fetchObject())
				if (is_dir($this->path . '/' . $resource->resource))
					$nodes[] =  $this->getChild($resource->resource);
			return $nodes;
		}

		//A PARTIR DU NIVEAU 4, IL FAUT TROUVER QUEL SERVICE GERE LA RESSOURCE EN QUESTION POUR CONSTRUIRE LE CHEMIN API
		$users_services = rest('https://api.' . getenv('DOMAIN') . '/optimus-base/users/' . $user_id . '/services', 'GET', null, $authBackend->token)->data;
		foreach($users_services as $service)
			if (is_array($service->managed_resources) AND in_array($path[4], $service->managed_resources))
				$current_service = $service->name;

		if (isset($current_service))
		{
			//AU NIVEAU 4, ON AFFICHE LES DOSSIERS QUI CONTIENNENT DES RESSOURCES AUXQUELLES L'UTILISATEUR PEUT ACCEDER EN LECTURE EN APPLICATION D'AU MOINS UN PARTAGE EXISTANT
			if (end($path) == $path[4])
			{
				$resources = rest('https://api.' . getenv('DOMAIN') . '/' . $current_service . '/' . $owner_id . '/' . $path[4], 'GET', null, $authBackend->token);
				if ($resources->code == 200)
				{
					$resources = $resources->data;
					foreach ($resources as $resource)
						if (!isset($resource->restrictions))
							$nodes[] = $this->getChild($resource->displayname);
						else if (!in_array('read', $resource->restrictions))
							$nodes[] = $this->getChild($resource->displayname);
						else
							throw new DAV\Exception\Forbidden('Permission denied');
					return $nodes;
				}
			}
	
			//AU NIVEAU 5 OU +, ON AFFICHE LE CONTENU UNIQUEMENT SI L'UTILISATEUR A ACCES A LA RESSOURCE
			if (sizeof($path) > 5)
			{
				$input = '{"filter": [{"field":"displayname", "type":"=", "value":"' . $path[5] . '"}]}';
				$resource = rest('https://api.' . getenv('DOMAIN') . '/' . $current_service . '/' . $owner_id . '/' . $path[4], 'GET', $input, $authBackend->token);
				if ($resource->code == 200 AND sizeof($resource->data) > 0 AND !in_array('read', get_restrictions($user_id, $owner_id, $path[4] . '/' . $resource->data[0]->id)))
				{
					$iterator = new \FilesystemIterator($this->path, \FilesystemIterator::CURRENT_AS_SELF | \FilesystemIterator::SKIP_DOTS);
					foreach ($iterator as $entry)
						$nodes[] = $this->getChild($entry->getFilename());
					return $nodes;
				}
				else
					throw new DAV\Exception\Forbidden('Permission denied');
			}
		}
		else
			return $nodes;
	}

	public function delete()
	{
		foreach ($this->getChildren() as $child)
			$child->delete();

		$path = explode('/', $this->path);
			
		global $server, $pdo, $authBackend;
		include_once('/srv/api/libs/functions.php');
		$owner_email = check('owner_email', $path[3], 'email', true);
		$user_email = check('owner_email', str_replace('principals/', '', $server->getPlugin('auth')->getCurrentPrincipal()), 'email', true);
		$owner_id = get_user_id($owner_email);
		$user_id = get_user_id($user_email);
		$admin = is_admin($user_id);

		//MEME les admins ne devraient pas avoir le droit de supprimer les ressources en direct ici
		//SI L'UTILISATEUR EST ADMIN, OU SI L'UTILISATEUR NAVIGUE DANS SES PROPRES FICHIERS, AUCUNE VERIFICATION N'EST NECESSAIRE
		if (!is_admin($user_id) AND $owner_id != $user_id)
		{
			$users_services = rest('https://api.' . getenv('DOMAIN') . '/optimus-base/users/' . $user_id . '/services', 'GET', null, $authBackend->token)->data;
			foreach($users_services as $service)
				if (is_array($service->managed_resources) AND in_array($path[4], $service->managed_resources))
					$current_service = $service->name;
			
			//HYPOTHESE 1 : ON EST DANS UN DOSSIER DE TYPE 'resource'
			if (isset($current_service))
			{
				if (sizeof($path) == 6)
					throw new DAV\Exception\Forbidden('Permission denied to delete in a resource folder. Please use API');
				else if (sizeof($path) > 6)
				{
					$input = '{"filter": [{"field":"displayname", "type":"=", "value":"' . $path[5] . '"}]}';
					$resource = rest('https://api.' . getenv('DOMAIN') . '/' . $current_service . '/' . $owner_id . '/' . $path[4], 'GET', $input, $authBackend->token);
					if ($resource->code != 200 OR sizeof($resource->data) == 0)
						throw new DAV\Exception\Forbidden('Permission denied. Unknown resource');
					else if (in_array('write', get_restrictions($user_id, $owner_id, $path[4] . '/' . $resource->data[0]->id)))
						throw new DAV\Exception\Forbidden('Permission denied to delete ' . $this->path);
				}
			}
			//HYPOTHESE 2 : ON EST DANS UN DOSSIER DE TYPE 'files'
			else
			{
				//ON VERIFIE S'IL EXISTE UNE AUTHORIZATION DE TYPE 'files'
				$resource_path = explode('/', $this->path);
				unset($resource_path[0]); unset($resource_path[1]); unset($resource_path[3]);
				$resource_path = implode('/', $resource_path);
				if (in_array('write', get_restrictions($user_id, $owner_id, $resource_path)))
					throw new DAV\Exception\Forbidden('Permission denied to delete ' . $this->path);
			}
		}

		rmdir($this->path);
		return true;
	}

	public function getQuotaInfo()
	{
		$total = disk_total_space(realpath($this->path));
		$free = disk_free_space(realpath($this->path));

		return [$total - $free, $free,];
	}

	public function moveInto($targetName, $sourcePath, DAV\INode $sourceNode)
	{
		if (!$sourceNode instanceof self && !$sourceNode instanceof File)
			return false;
		return rename($sourceNode->path, $this->path.'/'.$targetName);
	}
}